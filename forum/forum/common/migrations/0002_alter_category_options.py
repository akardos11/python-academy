# Generated by Django 3.2.9 on 2021-12-09 16:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'ordering': ['-order_number'], 'verbose_name': 'Category', 'verbose_name_plural': 'Categories'},
        ),
    ]
