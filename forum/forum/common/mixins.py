from typing import Optional, cast
from django.contrib.auth.mixins import UserPassesTestMixin


class SuperUserMixin(UserPassesTestMixin):
    def __init__(self):
        self.request = None

    def test_func(self) -> Optional[bool]:
        return cast(bool, self.request.user.is_superuser)
